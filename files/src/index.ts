import { ExpressRouter } from '@kominal/lib-node-express-router';
import { MongoDBInterface } from '@kominal/lib-node-mongodb-interface';
import { info, startObserver } from '@kominal/observer-node-client';
import { exit } from 'process';

let mongoDBInterface: MongoDBInterface;
let expressRouter: ExpressRouter;

async function start(): Promise<void> {
	startObserver();
	mongoDBInterface = new MongoDBInterface('controller');
	await mongoDBInterface.connect();
	expressRouter = new ExpressRouter({
		healthCheck: async (): Promise<boolean> => true,
		routes: [],
	});
	await expressRouter.start();
}
start();

process.on('SIGTERM', async () => {
	info("Received system signal 'SIGTERM'. Shutting down service...");
	expressRouter.getServer()?.close();
	await mongoDBInterface.disconnect();
	exit(0);
});
